# BOOKSEMBLER -- The BOOtstrapped Kmer asSEMBLER

*Licence: GPL v3, because I want to be able to modify and distribute
the source of derivative works from this that other people might
create.*

Okay, let's do this.

This is an assembler for long reads (the longer the better), built on
two core ideas:

1. Determining consensus by non-repeating kmers
2. Looking for overlaps as early as possible, exploiting a
   birthday-like problem

## Requirements

libgzstream - for processing compressed files via streams in c++
 [package in Debian: libgzstream-dev]

## Motivation

As far as I'm aware, these ideas are rarely (if at all) implemented in
other assemblers, which is why I'm putting these ideas out into the
open for discovery.

As always, if someone else wants to take a crack at this, please go
ahead; I've got plenty of other ideas to occupy my mind, and would
appreciate any help that can be given.

Update - September 2020: [MBG](https://github.com/maickrau/MBG)
appears to implement the non-repeating kmer pre-filter for assembly.

## Non-repeating kmers

Repetitive regions are a huge problem for assembly, so let's see how
this works if they are removed. This is much safer to do with long
reads, as *most* (but not all) repetitive regions have unit lengths of
less than 10kb (reference: some future paper that I will write if
anyone wants to fund repeat discovery research; currently based on my
"hobby" observations of any long-read assemblies that pique my
interest). My preferred kmer length for this is 17bp, based once again
on my own anecdotal observations of repetitive elements in DNA
sequences.

Locally non-repetitive kmers are easy: just scan a sequence, and
record any kmers that only appear once. Unfortunately this will also
include sequencing adapters and transposable elements, which would not
be great things to use for the purpose of overlap assembly. For this
reason the algorithm will also use a global table of counts for all
detected locally-unique kmers, and blacklist any kmers that appear in
"too many" different reads.

There's a little niggling problem associated with a massive abundance
of reverse-complement repeat structures in most genomes. In order to
reduce misassembly errors due to reverse-complement confusion,
sequences and kmers on different strands will be treated as distinct
haplotypes (contrary to almost every other assembly program).

The kmers for matching are chosen randomly, but in a consistent
fashion. This means that the same kmer on separate reads will be
selected, regardless of read composition.  My current idea for how to
do this is to create a hash function to map kmers to a random /
unpredictable transformation, and choose a proportion of the output
range as the kmers to keep.

## Assembling Birthdays

https://twitter.com/gringene_bio/status/1141672305627561984

If a single read represents a millionth of the genome, then it would
be expected that about a hundred matches would be found between
different reads after 10,000 reads had been looked at. When you get
10,000 reads, it's a cause for celebration, so let's have a match
party!

*[Note: this celebration threshold will change depending on the nature
of the source material.]*

By reducing the number of checkable reads, the speed of the match step
is increased, which means it can be done more often. This won't be a
single match step; it'll be a continual process of matching throughout
the course of the assembly, as reads are coming in.

Any discovered, high-confident matches have alignment and consensus
calculated to create a single longer contig, increasing the chance
that this contig will be hit during the next match party. The assembly
is considered complete when a desired proportion of reads match up
with assembled contigs; no more reads needed.

Note that the usual "overlap" step is broken into two phases: match
and alignment.  The match phase is just to determine quickly whether
there's a good chance two sequences have shared sequence, whereas the
alignment process involves the more time-consuming process of
alignment (presumably using the hashed unique kmers as anchors).

The actual implementation of this would involve a few hash tables. As
each read is loaded into the database, it's kmer hash set would be
recorded in a working pool via a (kmer -> sequence #) tuple. If there
are any kmers in the sequence that have been previously seen, then a
tally is incremented for all possible sequence pairs.  When a suitable
number of sequence pairs have been seen, any sequence pairs whose
count exceeds a particular threshold will be processed for alignment
and consensus.  This streamed approach should be a lot faster than an
all-vs-all matching, and could potentially distribute a bit of
computation time in between sucessive reads from the input files.

## Implementation Ideas

* Store assemblies on disk as a B-tree
  - sort order: (contig id, contig start location)
  - every input sequence starts out as its own contig; contigs can be
    merged by linking the start location of a contig to a different contig*

It is assumed that as sequence represents a physical molecule, either
single or double stranded (with a forward and reverse-complement
sequence).  Reverse complementation should *not* be assumed, either in
the assembled sequence or in individual sequences. The forward and
reverse-complement sequence do not necessarily need to match. This
will be especially the case when methylation is involved.

* Sequence information:
  - contig id
  - contig start location
  - header / id
  - sequence
  - quality
  - front soft clip length (prior to contig start location)
  - mid clip position [only for double-stranded sequence]
  - mid clip length, or reverse-complement start position
    [double-stranded sequence only]
  - end soft clip length (position of last mapped base)

To aid assembly, sequences should be represented as a
homopolymer-compressed encoding, with an RLE of the number of base
repeats. This means that single-base deletions and homopolymer length
adjustments won't affect the HP-compressed sequence. These issues are
the most common issues that I observe in nanopore sequencing; this is
not a coincidence.

i.e. `GATTACA => (GATACA, RLE(112111))`

Inserting sequences into a contig (i.e. prior to the start, somewhere
in the middle, or at the end) should not require updating any other
sequence metadata, especially if negative start locations are
allowed. The B-tree structure may need to be rebalanced with new
insertions.

Contig subsequences should be placed into a hash array to aid
assembly, mapping equal-length subsequences (e.g. 32-bp kmers) to the
*first* appearance of a sequence in the standard contig sort
order. When the recorded location of a sequence changes (i.e. change
in contig id and/or start location), all hash lookups for that
sequence will need to be updated. The hash array should store the
multiplicity of the kmer, which will help in identifying adapter
sequences, repeat regions, and incomplete information.

Shifting sequences around within an assembly could result in a hash
array with incomplete information, when the shifted sequence moves
from a smaller number contig id to a larger number contig id. To avoid
recalculating the entire array, when a sequence is removed or shifted
forward, the earliest position of that kmer should be deleted from the
hash array (if another alternative at or near that position cannot be
quickly found). As kmer multiplicity is recorded, incomplete
information can be distinguished from the absence of sequences with
that kmer in the array.
