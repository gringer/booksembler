#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <cmath>
#include <cctype>
#include <vector>
#include <fstream>
#include <thread>
#include <climits>
#include <iomanip>
#include "gzstream.h"

// value for a kmer that shouldn't be used
#define BAD_KMER ULLONG_MAX
// kmer size to use for hash array
#define KMER_SIZE 17
// proportion of kmers to retain (in 1024ths)
#define KMERPROP 102
// number of bits to shift to retain 10 bits (i.e. 0..1023)
#define KMER_BITSHIFT ((KMER_SIZE << 1) - 9)
// Number of threads (for parallel kmer counting)
#define NUM_THREADS 4

using namespace std;

enum FastXFileType { fasta, fastq, fastm };

typedef uint64_t kmer;

// Data structure to store sequence information
class SequenceRecord {
  const static uint32_t bitShift[];
  string seqID = "";
  string seq = "";
  vector<uint64_t> bitSeq;
  string qualID = "";
  string qual = "";
public:
  SequenceRecord() {
  }
  SequenceRecord(string s) {
    this->seq = s;
    if(s.length() < 20){
      this->seqID = s;
    } else {
      this->seqID = s.substr(0,10) + "..." + s.substr(s.length() - 10);
    }
    seqToBitSeq();
  }
  // A => 00; C => 01; G => 11; T=> 10
  static string decodeKmer(kmer k) {
    string res = "";
    for(size_t pos = 0; pos < (KMER_SIZE << 1); pos+=2){
      switch (k & 3L){
      case 0:
	res.push_back('A'); break;
      case 1:
	res.push_back('C'); break;
      case 3:
	res.push_back('G'); break;
      case 2:
	res.push_back('T'); break;
      }
      k >>= 2;
    }
    return(res);
  }
  static string decodeBits(kmer k) {
    string res = "";
    size_t bits = 0;
    while(k > 0 || (bits < KMER_SIZE)){
      res.push_back((k & 3L) + 0x30);
      k >>= 2;
      bits++;
    }
    return(res);
  }
  bool isEmpty(){
    return (seqID == "");
  }
  size_t length() const{
    return(seq.length());
  }
  string getSeq(){
    return (seq);
  }
  vector<uint64_t> getBitSeq(){
    return (bitSeq);
  }
  uint64_t getKmer(size_t pos) const{
    // DNA "hash" function, converts to 2bit representation
    // A => 00; C => 01; G => 11; T=> 10
    // This is done by a bitwise AND with b110 [i.e. 6]
    // Note: this assumes the kmer will only span at most two bits
    //       which could be violated for KMER_SIZE > 32
    if(pos > (seq.length() - KMER_SIZE)){
      return(BAD_KMER);
      cerr << "returned BAD kmer" << endl;
    }
    size_t binStart = pos >> 5;   // pos / 32
    size_t bitStart = (pos & 0x1fL) << 1; // (pos % 32) * 2
    size_t binEnd = (pos + KMER_SIZE) >> 5;
    size_t bitEnd = ((pos + KMER_SIZE) & 0x1fL) << 1L;
    uint64_t ret = 0;
    // cerr << "Pos: " << setw(4) << pos
    // 	 << "; +KMER_SIZE: " << (pos + KMER_SIZE)
    // 	 << "; bit start: " << setw(2) << bitStart
    // 	 << "; bit end: " << setw(2) << bitEnd;
    if(binStart == binEnd){
      ret = (bitSeq[binStart] >> bitStart) & ((1L << (KMER_SIZE << 1)) - 1L);
    } else {
      ret = (bitSeq[binStart] >> bitStart) +
	(((bitEnd == 0) ? 0 :
	  ((bitSeq[binEnd] & ((1L << bitEnd) - 1L))))
	 << ((KMER_SIZE << 1) - bitEnd));
    }
    // cerr << " [Ret: " << std::setw(15) << ret
    // 	 << "; Decoded: " << SequenceRecord::decodeKmer(ret) << "]" << endl;
    return(ret);
  }
  bool keep(size_t pos) const{
    // Determine whether or not the kmer starting at [pos] should be kept
    // only need to consider the five bases that are used [5 * 2 = 10 bits]
    uint64_t h = 0, posMod = 0,
      binStart = 0, bitStart = 0;
    for(int i = 4; i >= 0; i--){
      h <<= 2; // slide out old 2bits
      posMod = bitShift[i] + (pos << 1);
      binStart = posMod >> 6; // pos / 64
      bitStart = (posMod & 0x3fL); // pos % 64
      uint64_t newVal = ((bitSeq[binStart] >> bitStart) & 3L);
      h |= ((bitSeq[binStart] >> bitStart) & 3L);
    }
    return(h < KMERPROP);
  }
  string getQual(){
    return (qual);
  }
  string getID(){
    return (seqID);
  }
  string getShortID() const{
    return (seqID.substr(0, seqID.find_first_of(' ')));
  }
  bool hasQual(){
    return (qual != "");
  }
  void clear(){
    this->bitSeq.clear();
    this->seqID = "";
    this->seq = "";
    this->qualID = "";
    this->qual = "";
  }
  void seqToBitSeq(){
    uint64_t h = 0;
    size_t bitPos = 0;
    bitSeq.clear();
    for (char e : this->seq) {
      if(bitPos == 0){
        h |= (e & 6L) >> 1;
      } else {
        h |= (e & 6L) << (bitPos - 1);
      }
      bitPos += 2;
      if(bitPos >= 64){
        bitSeq.push_back(h);
        bitPos -= 64;
        h = 0;
      }
    }
    if(bitPos > 0){
        bitSeq.push_back(h);
    }
  }
  bool loadSequence(vector<string> &fastxBuf, FastXFileType fileType){
    if((fileType == fasta) && (fastxBuf.size() == 2)){
      this->seqID = fastxBuf[0].substr(1);
      this->seq = fastxBuf[1];
      seqToBitSeq();
      fastxBuf.clear();
      return (true);
    } else if((fileType == fastq) && (fastxBuf.size() == 4)){
      this->seqID = fastxBuf[0].substr(1);
      this->seq = fastxBuf[1];
      seqToBitSeq();
      this->qualID = fastxBuf[2].substr(1);
      this->qual = fastxBuf[3];
      fastxBuf.clear();
      return (true);
    } else { // partially-complete sequences will not be loaded
      string typeName =
	(fileType == fasta) ? "fasta" :
	(fileType == fastq) ? "fastq" :
	(fileType == fastm) ? "fastm" : "<none>";
      cerr << "Warning: attempted to load incomplete "
	   << typeName << " sequence of length "
	   << fastxBuf.size() << endl;
    }
    return (false);
  }
};
// bit shifts were created in R via:
// > sample(0:16) * 2
const uint32_t SequenceRecord::bitShift[] =
  { 32, 8, 12, 18, 30, 26, 28, 16, 2, 6, 0, 24, 14, 20, 4, 10, 22 };

// Collects unique kmers from a sequence and stores them in a hash set
void fetchUnique(unordered_map<kmer, size_t> &res,
		 unordered_map<kmer, size_t> &gCounts,
		 unordered_set<kmer> &resFilt,
		 const SequenceRecord s){
  res.clear();
  resFilt.clear();
  // a string shorter than the KMER_SIZE has no kmers
  if(s.length() < (KMER_SIZE)){
    return;
  }
  // Generate a list of all kmers in the sequence
  for(int i = 0; i < (s.length() - (KMER_SIZE) + 1); i++){
    if(s.keep(i)){
      res[s.getKmer(i)]++;
    }
  }
  // Filter out duplicated kmers (only add if it's been seen once)
  for(auto it : res){
    if(it.second == 1){
      resFilt.emplace(it.first);
      gCounts[it.first]++;
    }
  }
}

// Displays a sequence record as fastq or fasta, depending on whether
// or not quality information is included
void printOut(SequenceRecord seqRec){
  if(!seqRec.isEmpty()){
    if(seqRec.hasQual()){
      cout << "@" << seqRec.getID() << endl;
      cout << seqRec.getSeq() << endl;
      cout << "+" << endl;
      cout << seqRec.getQual() << endl;
    } else {
      cout << ">" << seqRec.getID() << endl;
      cout << seqRec.getSeq() << endl;
    }
  }
}

// Process a fastx vector array containing at most a single record plus
// the next header row. The output should be a properly-formatted complete
// record, or nothing.
SequenceRecord processFastx(vector<string> &fastxBuf, bool dumpRemainder){
  SequenceRecord retVal;
  if(fastxBuf.size() == 1){
    return (retVal);
  }
  FastXFileType fileType;
  switch (fastxBuf[0][0]) {
  case '#':
    fileType = fastm;
    break;
  case '@':
    fileType = fastq;
    break;
  default:
    fileType = fasta;
  }
  if(dumpRemainder){
      retVal.loadSequence(fastxBuf, fileType);
  } else {
    string lastLine = fastxBuf.back(); fastxBuf.pop_back();
    if(fileType == fasta){ // ** fasta logic **
      if(fastxBuf.size() == 1){ // [first] sequence line
	fastxBuf.push_back(lastLine); // restore line
      } else if(lastLine[0] == '>'){ // start of a new sequence
	retVal.loadSequence(fastxBuf, fileType);
	fastxBuf.push_back(lastLine); // restore line
      } else { // a continuation of the sequence
	fastxBuf.back() += lastLine; // add to sequence line
      }
    } else if (fileType == fastq) { // ** fastq logic **
      switch (fastxBuf.size()) {
      case 0: // header line
      case 1: // [first] sequence line
      case 3: // [first] quality line
	fastxBuf.push_back(lastLine);
	break;
      case 2:
	if(lastLine[0] == '+'){ // reached end of sequence
	  fastxBuf.push_back(lastLine);
	} else { // allow multi-line fastq
	  fastxBuf.back() += lastLine; // add to sequence line
	}
	break;
      case 4:
	if(fastxBuf[1].size() <= fastxBuf[3].size()){ // finished sequence
	  retVal.loadSequence(fastxBuf, fileType);
	  fastxBuf.push_back(lastLine); // restore line
	} else { // add to quality line
	  fastxBuf.back() += lastLine; // add to sequence line
	}
	break;
      default:
	cerr << "Warning: unexpected fastq condition; clearing buffer\n";
	fastxBuf.clear();
      }
    } else {
      cerr << "Warning: unexpected stream condition; clearing buffer\n";
      fastxBuf.clear();
    }
  }
  return (retVal);
}

// Displays the unique kmers in a sequence
void showUniqueKmers(unordered_map<kmer, size_t> &res,
		     unordered_map<kmer, size_t> &gCounts,
		     unordered_set<kmer> &uniqueKmers,
		     unordered_map<kmer, size_t> &seenKmers,
		     SequenceRecord s){
  cout << "Unique kmers for '" << s.getShortID() << "':" << endl;
  fetchUnique(res, gCounts, uniqueKmers, s);
  size_t printed = 0;
  size_t repeatsSeen = 0;
  for(kmer k : uniqueKmers){
    cout << k;
    auto it = seenKmers.find(k);
    if(it == seenKmers.end()){
      cout << " ";
      seenKmers[k] = 1;
    } else {
      cout << "*";
      it->second++;
      repeatsSeen++;
    }
    if(printed++ % 5 == 4){
      cout << endl;
    } else {
      cout << " ";
    }
  }
  //cout << endl;
  cout << "Number of unique kmers seen in previous sequences: "
       << repeatsSeen << endl;
}

// Processes kmers into the kmer record structure, noting if any have been
// seen before
size_t
processUniqueKmers(const unordered_set<kmer> &uniqueKmers,
		   unordered_map<kmer, size_t> &gCounts,
                   unordered_map<kmer, string> &seenKmers,
		   unordered_map<kmer, vector<string>> &dupKmers,
		   SequenceRecord s){
  string k;
  unordered_map<string, size_t> rptCounts;
  k.resize((KMER_SIZE));
  cout << "Unique kmers for '" << s.getShortID() << "', seen in other kmers: ";
  size_t printed = 0;
  size_t repeatsSeen = 0;
  size_t gCountTotal = 0;
  size_t kmerTotal = 0;
  for(auto i : uniqueKmers){
    // There are a lot of iterators used here [i.e. "auto <abbrev>it"]
    // This reduces the number of hash lookups requred when changing
    // an item within the hash maps
    auto skit = seenKmers.find(i);
    if(skit == seenKmers.end()){
      seenKmers[i] = s.getShortID();
    } else {
      string lastID = skit->second;
      skit->second = s.getShortID();
      auto dkit = dupKmers.find(i);
      if(dkit == dupKmers.end()){
	dkit = dupKmers.emplace(i, vector<string>({lastID})).first;
      }
      if(dkit->second.size() < 10){
	for(auto dkitv: dkit->second){
	  rptCounts[dkitv]++; // set up repeat pairing
	}
	dkit->second.push_back(s.getShortID()); // add to end of duplicates array
      }
      kmerTotal++;
      gCountTotal += gCounts[i];
      repeatsSeen++;
    }
  }
  size_t maxRepeats = 0; string maxID = "";
  for(pair<const string, size_t> i: rptCounts){
    if(i.second > maxRepeats){
      maxRepeats = i.second;
      maxID = i.first;
    }
  }
  cout << repeatsSeen << endl;
  if(maxRepeats > 100){
    cout << "  mean global count: " << setw(3) << ((float)gCountTotal / kmerTotal) << endl;
    cout << "  max: " << s.getShortID() << " " << maxID << " " << maxRepeats << endl;
    // cout << "  all unique kmers for " << id << ":" << endl;
    // for(auto i : uniqueKmers){
    //   std::copy(i.begin(), i.end(), k.begin());
    //   cout << "    " << k << "  " << KHasher::hash(i) << endl;
    // }
  }
  return(maxRepeats);
}


int main(int argc, char* argv[]){
  unordered_map<kmer, string> seenKmers;
  unordered_map<kmer, size_t> kCounts;
  unordered_map<kmer, size_t> globalCounts;
  unordered_set<kmer> kFiltered;
  unordered_map<kmer, vector<string>> dupKmers;
  for(size_t argi = 1; argi < argc; argi++){
    string s = argv[argi];
    fstream fastaFileFS(s);
    string buf;
    vector<string> fastxBuf;
    SequenceRecord seqRec;
    if(fastaFileFS.good()){ // if the argument is a file
      igzstream fastaFile(s.c_str()); // reload using gzstream
      cout << "file '" << s << "':\n-- BEGIN --" << endl;
      while(fastaFile.good()){
	getline(fastaFile, buf);
	if(fastaFile.good()){
	  fastxBuf.push_back(buf);
	}
	seqRec = processFastx(fastxBuf, !fastaFile.good());
	if(!seqRec.isEmpty()){
	  fetchUnique(kCounts, globalCounts, kFiltered, seqRec);
          processUniqueKmers(kFiltered, globalCounts, seenKmers, dupKmers,
                             seqRec);
	}
      }
      cout << "-- END --" << endl;
    } else { // assume the argument is a DNA sequence
      fetchUnique(kCounts, globalCounts, kFiltered, SequenceRecord(s));
      processUniqueKmers(kFiltered, globalCounts,
			 seenKmers, dupKmers, SequenceRecord(s));
    }
  }
}
