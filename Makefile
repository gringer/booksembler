test: booksembler
	# test a single sequence, repeated
	./booksembler 'TTGTGGCAAGAGTCTGG' 'TTGTGGCAAGAGTCTGG'
	# test small sequence with a small number of non-repeated kmers
	./booksembler 'TTGTGGCAAGAGTCTGGTTGTGGCAAGAGTCTGGTTGTGGCAAGAGT'
	# test multiple command-line arguments, fasta file loading
	./booksembler simple_example.fasta 'TTATAGTCTGTATCTAAAACTACACTATTTGTGGCAAGAGTCTGGCCTTAATAATAGTCTGTATCTAAAACTACACTATTTGTGGCAA'
	# test gzipped fastq loading (2 reads with no intersecting kmers)
	./booksembler Nb_2reads_noMatch.fastq.gz
	# test 2 reads with partial / interrupted match
	./booksembler Nb_2reads_interruptedMatch.fastq.gz
	# test sequence shorter than KMER_LENGTH
	./booksembler 'ATATAGCT'
longtest: booksembler
	# test 7.5k dataset
	./booksembler Nb_7.5k.fastq.gz
booksembler: booksembler.cc
	g++ -Wfatal-errors -g booksembler.cc -lgzstream -lz -lpthread -o booksembler
clean:
	rm -f ./booksembler *~
