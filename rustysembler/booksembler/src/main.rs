use std::io;
use std::io::BufReader;
use std::io::prelude::*;
use std::fs::File;
use std::collections::HashMap;
use flate2::read::GzDecoder;

// bit shifts for hashing were created in R via:
// > sample(0:31) * 2
// const BIT_SHIFTS: [u8; 32] = [
//    50, 12, 36, 60, 32, 02, 10, 34,
//    62, 52, 58, 28, 20, 26, 40, 56,
//    00, 16, 38, 30, 08, 48, 04, 24,
//    14, 06, 46, 44, 18, 42, 22, 54];
// [possibly unnecessary]

struct KmerPos {
    first_pos: u64,
    first_contig: u32,
    count: u32,
}

struct SequenceRecord {
    seq_id: String,
    seq: String,
    len: u64,
    bit_seq: Vec<u64>,
    qual_id: String,
    qual: String,
}

// Convert sequence as string to bit-packed number (case insensitive)
// To make complementation simpler, the base encoding is as follows:
// A: 0; C: 1; G: 3; T: 2
// e.g. GA TT AC AA -> 0x04a3 [with MSB encoding]
fn as_bits(seq: &str) -> Vec<u64> {
    let mut h:u64 = 0;
    let mut bit_pos = 0;
    let mut bit_seq:Vec<u64> = Vec::new();
    for e in seq.bytes() {
	if bit_pos == 0 {
            h |= (e as u64 & 6) >> 1;
	} else {
            h |= (e as u64 & 6) << (bit_pos - 1);
	}
	bit_pos += 2;
	if bit_pos >= 64 {
            bit_seq.push(h);
            bit_pos -= 64;
            h = 0;
	}
    }
    if bit_pos > 0 {
        bit_seq.push(h);
    }
    return bit_seq;
}

fn decode_chunk(mut h: u64) -> String {
    let mut bases_seen:u8 = 0;
    let mut res:String = String::from("");
    while h > 0 {
	res.push(
	    match h & 3 {
		0 => 'A',
		1 => 'C',
		2 => 'T',
		3 => 'G',
		_ => 'N'
	    });
	h >>= 2;
	bases_seen += 1;
    }
    while bases_seen < 32 {
	res.push('A');
	bases_seen += 1;
    }
    return res;
}

fn decode_bits(bit_seq: Vec<u64>) -> String {
    let mut res:String = String::from("");
    for h in bit_seq {
	res.push_str(&decode_chunk(h));
    }
    return res;
}

fn update_hash(sr: SequenceRecord, hm: &mut HashMap<u64, KmerPos>) {
    let mut seq_pos:u64 = 63; // points to *last* base
    let mut last_chunk:u64 = sr.bit_seq[0];
    let mut current_chunk:u64;
    let mut added_chunks = 0;
    for h in sr.bit_seq {
	current_chunk = h;
	if added_chunks > 0 {
	    for bit_shift in (0..63).step_by(2) {
		println!("Inserting {} at position {}", decode_chunk(last_chunk), seq_pos);
		if seq_pos >= sr.len {
		    break;
		}
		// TODO: work out the correct insertion
		hm.insert(last_chunk, KmerPos {
		    first_pos: seq_pos,
		    first_contig: last_chunk as u32,
		    count: 1,
		});
		last_chunk >>= 2;
		// TODO: check to see if this makes sense and can be simplified
		last_chunk |= ((current_chunk >> bit_shift) & 3) << (62);
		seq_pos += 1;
	    }
	}
	last_chunk = h;
	added_chunks += 1;
    }
}

fn main() -> io::Result<()> {
    let file_name = "../../Nb_2reads_interruptedMatch.fastq.gz";
    let buf_reader = BufReader::new(GzDecoder::new(
	File::open(file_name).unwrap()));
    let mut lc = 0;
    let mut sr: SequenceRecord;
    let mut fastx_buf: Vec<String> = Vec::new();
    let mut kmer_map: HashMap<u64, KmerPos> = HashMap::new();
    for line in buf_reader.lines() {
	let line_val = line.unwrap();
	fastx_buf.push(line_val.clone());
	if fastx_buf.len() >= 4 {
	    sr = SequenceRecord {
		bit_seq: Vec::new(),
		qual: fastx_buf.pop().unwrap(),
		qual_id: fastx_buf.pop().unwrap(),
		seq: fastx_buf.pop().unwrap(),
		len: 0,
		seq_id:fastx_buf.pop().unwrap(),
	    };
	    sr.bit_seq = as_bits(&sr.seq);
	    sr.len = sr.seq.len() as u64;
	    println!("0: {}", sr.seq_id);
	    println!("1: {}", sr.seq);
	    print!("e: ");
	    for e in &sr.bit_seq {
		for bp in (0 .. 64).step_by(4) {
		    print!("{:x}", (e >> bp) & 0xf);
		}
		print!(" ");
	    }
	    print!("\n");
	    println!("d: {}", decode_bits(sr.bit_seq.clone()));
	    println!("2: {}", sr.qual_id);
	    println!("3: {}", sr.qual);
	    update_hash(sr, &mut kmer_map);
	    fastx_buf.clear();
	}
	lc += 1;
    }
    println!("Read {} lines", lc);
    Ok(())
}
